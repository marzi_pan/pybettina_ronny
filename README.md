# pyBettina
A model to simulate tree population dynamics in python.

# License and copyright

Copyright (c) 2015-2018, OpenGeoSys Community (http://www.opengeosys.org) Distributed under a Modified MIT License. See accompanying file LICENSE.txt or http://www.opengeosys.org/project/license.
