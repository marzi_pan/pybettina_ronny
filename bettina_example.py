#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example script in order to demonstrate how bettina might be used. The
parameters are explained below.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""
if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    print("jo")
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from pybettina import Land
from pybettina import Flora
from pybettina import Bettina

# working_directory: Directory, where simulation results are saved. Please make
# sure the location exists on your local machine -- type:string
working_directory = "./tests/"
# model_name: This string is contained in all output files generated
# -- type:string
model_name = "example_model"

# length_x,y: the dimensions (in m) of the simulated land domain
# -- type:double,doube
length_x, length_y = 10, 10
# layers_x,y: the number of layers in each dimension. -- type:int, int
layers_x, layers_y = 50, 50
# origin_x,y: the coordinates of the upper-left-bottom corner of the land
# mesh. -- type:double, double
origin_x, origin_y = 0, 0

# pressure_initial_name, concentration_initial_name: names for the initial con-
# ditions for the primary variables -- type:string
concentration_initial_name = "c_ini"
pressure_initial_name = "p_ini"
# darcy_velocity_initial_name: name of the initial darcy velocity distribution.
# Necessary to define 2nd type boundary conditions -- type:string
darcy_velocity_initial_name = "q_ini"
# node_id_name: name of the property vector containing the node ids. Necessary
# for ogs to run properly. -- type:string
node_id_name = "bulk_node_ids"
# pressure_variable_name, concentration_variable_name: Names of the primary
# variables themselves -- type:string
concentration_variable_name = "concentration"
pressure_variable_name = "pressure"

# tree_species_names: list containing the species which considered in the
# initial tree distribution. Given in the form: [species1, species2, ...],
# with species_i being a string with the species name -- type:list of strings
tree_species_names = ["Avicennia"]
# initial_plants: number of individums planted for each species for the initial
# plant distributions. Given in the form [n_species1, n_species2, ...], with
# n_speciesi being the number of individuums of each species
# -- type:list of strings
initial_plants = [100]

# ini_darcy/pressure/concentration_function(point): functions to create intial
# conditions. Note: all functions must depend on point, which is a (3,1) tuple
# in order to allow for spatially depending initial distributions
#  -- type:python function decleration

# number_of_bettina_timesteps: total number of iterations of the bettina model
# -- type:int

def ini_darcy_function(point):
    return 0


def ini_pressure_function(point):
    return 0


def ini_concentration_function(point):
    return 0.035


number_of_bettina_timesteps = 200
# bettina_delta_t: length of one timestep in bettina in [s]. Note: one day
# corresponds to 86400 seconds -- type:double
bettina_delta_t = 356.25/4. * 86400

# vtu_file_postfix: last characters of all vtu files -- type:string
vtu_files_postfix = ".vtu"

# # # # # # # # End of parameter definition

# # # Land domain definition
land = Land.Land(model_name + "_land", working_directory)
# # Creation of 2D patch
land.create2DRectangularLand(
        model_name + "_land_mesh", origin_x, origin_y,
        length_x, length_y, layers_x + 1, layers_y + 1)
n = land.initial_mesh.grid.GetNumberOfPoints()
# # Initial conditions for land patch
c, p = land.setCIniPIniAndNodeIds(
        concentration_initial_name, pressure_initial_name,
        darcy_velocity_initial_name, node_id_name,
        ini_darcy_function, ini_pressure_function, ini_concentration_function)
# # Property naming
land.setCurrentPropertyNames(
        [concentration_variable_name, pressure_variable_name])
# # Output of grid file to hard drive
land.outputLand()

# # # Plant population definition
flora = Flora.Flora(model_name + "_flora", land, working_directory)
# # Initial plant distribution, here the trees are planted randomly
flora.randomlyPlantTreesInRectangularDomain(
        initial_plants, tree_species_names, land.bounding_box)

# # # Bettina model definition using the land and flora object defined above
bettina = Bettina.Bettina(model_name + "_bettina", land, flora)

bettina.createFloraCollection(model_name + "_flora_grid", vtu_files_postfix)
bettina.createTreeCollection(tree_species_names, vtu_files_postfix)
# # Timeloop for bettina simulation
for i in range(number_of_bettina_timesteps):
    bettina.setConstantSubsurfaceProperties(
            [i * bettina_delta_t, (i + 1) * bettina_delta_t], [c, p])
    bettina.evolveSystem()
