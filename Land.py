#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Land object. This class contains all information about the nonbiotic land
domain: The mesh object itself is stored in this. Addiotionally information of
nutrient, flow velocity and hydrostatic pressure distribution within the mesh
is accessible from here. This class implies functionalities to create and
update the land mesh, e.g. grid spacing, and distribuitons of parameters and
variables according to environmental conditions.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""
from pymeshinteraction import MeshInteractor
from pymeshinteraction import MeshReaderAndAnalyser
from pymeshinteraction import MeshPointFinder as MPF
import numpy as np


class Land:
    def __init__(self, land_name, directory):
        self.initial_mesh_name = land_name
        self.working_directory = directory
        self.subsurface_properties = []

    def setInitialLandMesh(self, mesh):
        self.initial_mesh = mesh
        self.tini_mesh_name = mesh.meshName

    def setSurfacePointLocations(self):
        land_grid = self.initial_mesh.grid
        origin = [self.initial_mesh.ox, self.initial_mesh.oy,
                  self.initial_mesh.oz + self.initial_mesh.lengthZ]
        deltas = [self.initial_mesh.dx, self.initial_mesh.dy,
                  self.initial_mesh.dz]
        steps = [self.initial_mesh.extension_points_X,
                 self.initial_mesh.extension_points_Y, 1]
        boundary_mesh_name = (self.initial_mesh.meshName + "_" +
                              "top" + "Surface")
        boundary_creator = MPF.MeshPointFinder(land_grid)
        points, ids = boundary_creator.findPointsOnPlane(
                steps[0], steps[1], steps[2],
                deltas[0], deltas[1], deltas[2],
                origin[0], origin[1], origin[2])
        temp_boundary = MeshInteractor.MeshInteractor(boundary_mesh_name)
        temp_boundary.CreateMeshFromPoints([points, ids])
        temp_boundary.CreateMultipleTriangles()
        cells = temp_boundary.grid.GetCells()
        self.surface = MeshInteractor.MeshInteractor(boundary_mesh_name)
        self.surface.CreateMeshFromPoints([points, ids])
        self.surface.grid.SetCells(5, cells)
        self.surface.setDxDyDz(
                self.initial_mesh.dx,
                self.initial_mesh.dy,
                self.initial_mesh.dz)
        self.surface.OutputMesh(self.working_directory)

    def setCurrentPropertyNames(self, property_names):
        self.property_names = []
        for property_name in property_names:
            self.property_names.append(property_name)

    def extractSubsurfacePropertiesFromFiles(self):
        # This function will return a list of format
        # [[time,[[name1, array1], [...,...]]], [[...],[[...],..]]
        # containing the names of properties, their values across the grid and
        # the information about the time, where the values have been recorded
        self.subsurface_properties = []
        for filename in self.subsurface_files_names:
            self.subsurface_properties.append(
                    self.getTimeAndPropertyVectorsFromFile(
                            self.subsurface_files_directory,
                            filename, self.property_names))
        self.variable_land_properties = True

    def setConstantSubsurfaceProperties(self, times, properties):
        self.subsurface_properties = [times, properties]
        self.variable_land_properties = False

    def getSubsurfaceProperties(self):
        return self.subsurface_properties

    def updateLandMesh(self):
        try:
            self.subsurface_files_names[-1]

            self.initial_mesh_name = self.subsurface_files_names[-1][:-4]

            reader = MeshReaderAndAnalyser.MeshReaderAndAnalyser(
                        self.working_directory +
                        self.initial_mesh_name + ".vtu")
            reader.readMesh()
            self.initial_mesh.grid = reader.meshData
            self.initial_mesh.meshName = self.initial_mesh_name
            point_data = self.initial_mesh.grid.GetPointData()
            for i in range(len(self.property_names)):

                array = point_data.GetArray(self.ini_names[i])
                new_ini = point_data.GetArray(self.property_names[i])
                array.ShallowCopy(new_ini)
                array.SetName(self.ini_names[i])
            self.outputLand()
            print("LandMesh is successfully updated.")
        except AttributeError:
            print("Constant subsurface meshes are used.")
            print("Nothing to update within the aquifer.")

    def setSubsurfaceFileInformation(self, directory, filenames):
        self.subsurface_files_directory = directory
        self.subsurface_files_names = filenames

    def getTimeAndPropertyVectorsFromFile(
            self, directory, filename, propertynames):
        reader = MeshReaderAndAnalyser.MeshReaderAndAnalyser(
                            directory+filename)
        reader.readMesh()
        properties = []
        for propertyname in propertynames:
            properties.append(
                    [propertyname,
                     reader.getUnweightedPropertyArray(propertyname)])
        time = float(filename.strip(".vtu").split("_t_")[1])

        return time, properties

    def create3DRectangularLand(
            self, name, ox, oy, oz, lx, ly, lz, partsx, partsy, partsz):
        self.initial_mesh = MeshInteractor.MeshInteractor(name)
        self.initial_mesh.create3DRectangularGrid(
                partsx, ox, lx, partsy, oy, ly, partsz, oz, lz)
        self.initial_mesh.fillRectangularGridWithVoxels()
        self.bounding_box = [ox, ox + lx, oy, oy + ly, oz, oz + lz]
        self.tini_mesh_name = name

    def create3DDelaunayLand(
            self, name, ox, oy, oz, lx, ly, lz, partsx, partsy, partsz):
        self.initial_mesh = MeshInteractor.MeshInteractor(name)
        self.initial_mesh.create3DDelaunayGrid(
                partsx, ox, lx, partsy, oy, ly, partsz, oz, lz)
        self.bounding_box = [ox, ox + lx, oy, oy + ly, oz, oz + lz]
        self.tini_mesh_name = name

    def create2DRectangularLand(self, name, ox, oy, lx, ly, partsx, partsy):
        self.initial_mesh = MeshInteractor.MeshInteractor(name)
        self.initial_mesh.create3DRectangularGrid(
                partsx, ox, lx, partsy, oy, ly, 1, 0, 0)
        self.initial_mesh.fillRectangularPlanarGridWithQuads()
        self.bounding_box = [ox, ox + lx, oy, oy + ly, 0, 0]
        self.tini_mesh_name = name
        self.surface = self.initial_mesh

    def setCIniPIniAndNodeIds(
            self, c_name, p_name, q_name, node_id_name,
            darcy_velocity_function,
            pressure_function, concentration_function):
        n = self.initial_mesh.grid.GetNumberOfPoints()
        c, p, q, iD = [], [], [], []
        for i in range(n):
            point = self.initial_mesh.grid.GetPoints().GetPoint(i)

            q.append(darcy_velocity_function(point))
            p.append(pressure_function(point))
            c.append(concentration_function(point))
            iD.append(i)

        self.initial_mesh.AddPropertyVector(np.array(p), p_name, "double")
        self.initial_mesh.AddPropertyVector(np.array(q), q_name, "double")
        self.initial_mesh.AddPropertyVector(np.array(c), c_name, "double")
        self.initial_mesh.AddPropertyVector(
                np.array(iD), node_id_name, "unsigned_long")
        self.ini_names = [c_name, p_name, q_name]
        return np.array(c), np.array(p)

    def outputLand(self):
        self.initial_mesh.OutputMesh(self.working_directory)

    def resetCanopyHeight(self):
        N = self.initial_mesh.grid.GetNumberOfPoints()
        self.canopy_height = [[] for _ in range(N)]
