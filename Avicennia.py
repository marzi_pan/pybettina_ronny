#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
List containing species specific tree model parameters.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""


class Avicennia:
    def __init__(self):
        self.treeType = "Avicennia"
        self.age = 0
        self.treeTypeDouble = 1  # Needed, if two species are considered
        self.minSeedingAge = 20  # Not needed yet, probably needed later
        self.minSeedingHeight = 10  # Not needed yet, probably needed later
        self.minSeedingResources = 1e-3  # Not needed yet, maybe later
        self.seedsPerUnitArea = 0.3  # Not needed yet, probably needed later
        self.iniRootDepth = 0.004
        self.iniCrownHeight = 0.004
        self.iniStemRadius = 0.005
        self.iniStemHeight = 0.05
        self.iniRootRadius = 0.2
        self.iniCrownRadius = 0.2
        self.iniMinimumLeafWaterPotential = -7860000
        self.iniKfSap = 1.6e-10
        self.iniLP = 0.33e-14
        self.iniKgeom = 4000
        self.iniHalfMaxHeightGrowthWeight = 0.15
        self.iniMaintenanceFactor = 6e-7
        self.iniSunC = 8e-8
        self.iniGrowthFactor = 0.0015
        self.HeightSigmoSlope = 0.5
        self.SigmoSlope = 0.15
        self.Lifethresh = 0.25   # Needed for patial dieback
