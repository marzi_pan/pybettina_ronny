#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Redundant dummy script. It can be used later in order to define global
constants.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""


class Constants:
    def __init__(self):
        # 30000000 Jm-2 = 14.6 kg m^-2, Solarkonstante
        # (Energie, die Wasservedunstung bereitstellt)
        self.r_solar = 15
        self.k_rel = 0.25  # Eichung der Solarstrahlung???
