#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
TODO: Documentation update.
Tree object. This class contains all functionalities of trees like water
uptakee, growth, calculation of resistances, attribuation of root meshes,
parameter definition, plant definition, resource calculation, tree allometry
analyzation, tree growth weigth functions.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""

import numpy as np
from pymeshinteraction import MeshInteractor
from pymeshinteraction import MeshPointFinder as MPF


class Tree:
    def __init__(self, x, y, land, species, iD, flora_name):
        self.iD = iD
        self.root_mesh_prefix = str(iD) + species + "_" + flora_name
        self.root_mesh_name = (self.root_mesh_prefix + "_t_"+"%010d") % 0

        if species == "Avicennia":
            from . import Avicennia
            properties = Avicennia.Avicennia()
        self.treeType = properties.treeType
        self.age = properties.age
        self.tree_type_double = properties.treeTypeDouble
        self.min_seeding_age = properties.minSeedingAge
        self.min_seeding_height = properties.minSeedingHeight
        self.min_seeding_resources = properties.minSeedingResources
        self.seeds_per_unit_area = properties.seedsPerUnitArea
        self.root_d_layer = properties.iniRootDepth
        self.crown_d_layer = properties.iniCrownHeight
        self.stem_radius = properties.iniStemRadius
        self.stem_height = properties.iniStemHeight
        self.root_radius = properties.iniRootRadius
        self.crown_radius = properties.iniCrownRadius
        self.Psi_leaf_min = properties.iniMinimumLeafWaterPotential
        self.k_f_sap = properties.iniKfSap
        self.L_P = properties.iniLP
        self.k_geom = properties.iniKgeom
        self.halfmax_heightgrowthweight = \
            properties.iniHalfMaxHeightGrowthWeight
        self.maintenance_factor = properties.iniMaintenanceFactor
        self.land_mesh = land.surface
        self.tree_center = np.array([x, y])
        self.sun_c = properties.iniSunC
        self.growth_factor = properties.iniGrowthFactor
        self.height_sigmo_slope = properties.HeightSigmoSlope
        self.sigmo_slope = properties.SigmoSlope
        self.transmission_through_canopy = .6  # checken, wozu?
        self.lifecirc = 1.0
        self.lifethresh = properties.Lifethresh
        self.death = False
        self.unshaded = 1.0

    def plantTree(self, directory):
        self.directory = directory
        self.createRootMesh()
        self.updateCrownList()
        self.addRootParametersToRootMesh()
        self.root_mesh.OutputMesh(self.directory)
        self.water_flux = 0
        self.solar_energy = 0
        self.available_resources = 0

    def evolve(self, t_ini, t_end):
        self.treeMaintenance(t_end - t_ini)
        self.growthResources()
        self.checkForDeath()
        if not (self.death):
            self.treeGrowthWeights()
            self.treeGrowth()
            self.updateRootMesh(t_end)
            self.updateCrownList()

    def setShadowCoefficient(self, shadow_coefficient):
        self.shadow_coefficient = shadow_coefficient

    def gatherResources(self, delta_t, salinity_values, pressure_values):
        self.water_flux += self.calculateWaterFlux(
                delta_t, salinity_values, pressure_values)
        self.solar_energy += self.calculateSolarResources(delta_t)

    def rootSurfaceResistance(self):
        self.root_surface_resistance = ( 1 / self.lifecirc / self.L_P /
                self.k_geom / np.pi / self.root_radius**2 / self.root_d_layer)

    def xylemResistance(self):
        self.flow_length = (
                2 * self.crown_radius + self.stem_height +
                0.5 ** 0.5 * self.root_radius * self.lifecirc ** 0.5)
        self.xylem_resistance = (
                self.flow_length / self.k_f_sap /
                np.pi / self.stem_radius ** 2)

    def deltaPsi(self, salinity_values):
        self.delta_psi = (
                self.Psi_leaf_min +
                (2 * self.crown_radius + self.stem_height) * 9810)
        nodal_areas = self.root_mesh.getPropertyVector("nodal_areas")
        total_nodal_area = nodal_areas.sum()
        i = 0
        for ID in self.root_mesh_node_ids:
            self.delta_psi += (
                    85000 * salinity_values[ID] *
                    1000 * (nodal_areas[i] / total_nodal_area))
            i += 1
        # verstehe ich nicht, aber wird schon stimmen

    def calculateWaterFlux(self, delta_t, salinity_values, pressure_values):
        self.rootSurfaceResistance()
        self.xylemResistance()
        self.deltaPsi(salinity_values)
        water_uptake = ((- delta_t * self.delta_psi /
                         (self.root_surface_resistance+self.xylem_resistance)))
        return water_uptake

    def calculateSolarResources(self, delta_t):
        maximum_solar_energy = (
                self.lifecirc * np.pi * self.crown_radius ** 2 *
                self.sun_c * delta_t)
        solar_resources = (1 - self.shadow_coefficient) * maximum_solar_energy
#        self.solar_resources = (self.unshaded * maximum_solar_energy)
#  später gucken wegen Variante zur oberirdischen Konk.
        return solar_resources
#  warum hier und drüber irgend so eine Summe?

    def treeMaintenance(self, delta_t):
        self.treeVolume()
        self.maint = self.volume * self.maintenance_factor * delta_t

    def treeVolume(self):
        self.volume = self.lifecirc * (
                self.root_d_layer * np.pi * self.root_radius**2 +
                self.flow_length * np.pi * self.stem_radius**2 +
                self.crown_d_layer * np.pi * self.crown_radius ** 2)

    def growthResources(self):
        self.available_resources = min(self.solar_energy, self.water_flux)
        self.grow = (self.growth_factor *
                     (self.available_resources - self.maint))
        if(self.grow < 0):
            self.grow = 0
            self.lifecirc *= (self.available_resources / self.maint)

    def checkForDeath(self):
        self.death = False
        if (self.lifecirc < self.lifethresh):
            self.death = True

    def treeGrowth(self):
        self.crown_radius += (
                self.weight_crowngrowth * self.grow /
                (2 * np.pi *
                 (self.crown_d_layer * self.crown_radius * self.lifecirc**0.5 +
                  self.stem_radius ** 2 * self.lifecirc)))
        self.stem_height += (
                self.weight_stemgrowth * self.grow /
                (np.pi * self.stem_radius ** 2 * self.lifecirc))
        self.root_radius += (
                self.weight_rootgrowth * self.grow /
                (2 * np.pi * self.root_radius * self.lifecirc**0.5 * 
                 self.root_d_layer +
                 0.5 ** 0.5 * np.pi * self.stem_radius ** 2 * self.lifecirc))
        self.stem_radius += (
                self.weight_girthgrowth * self.grow /
                (2 * np.pi * self.stem_radius * self.lifecirc * 
                        (2 * self.crown_radius + self.stem_height +
                        0.5**0.5 * self.root_radius * self.lifecirc**0.5)))

    def treeGrowthWeights(self):
        self.weight_stemgrowth = (
                self.halfmax_heightgrowthweight /
                (1 + np.exp(- (self.crown_radius - self.root_radius) /
                            (self.crown_radius+self.root_radius) /
                            self.height_sigmo_slope)))
        self.weight_crowngrowth = (
                (1-self.weight_stemgrowth) /
                (1 + np.exp((self.solar_energy - self.water_flux) /
                 (self.solar_energy + self.water_flux) /
                 self.sigmo_slope)))

        self.weight_girthgrowth = (
                (1 - self.weight_stemgrowth - self.weight_crowngrowth) /
                (1 + np.exp((self.root_surface_resistance -
                             self.xylem_resistance) /
                 (self.root_surface_resistance + self.xylem_resistance) /
                 self.sigmo_slope)))

        self.weight_rootgrowth = (1 - self.weight_stemgrowth -
                                  self.weight_crowngrowth -
                                  self.weight_girthgrowth)

    def updateRootMesh(self, t):
        self.root_mesh_name = (self.root_mesh_prefix + "_t_" + "%015d") % t
        self.createRootMesh()
        self.addRootParametersToRootMesh()
        self.root_mesh.OutputMesh(self.directory)

    def updateCrownList(self):
        searchTree = MPF.MeshPointFinder(self.land_mesh.grid)
        dx, dy = self.land_mesh.dx, self.land_mesh.dy
        # todo: überflüssig
        pointsTree = searchTree.findPointsWithinRadius(
                [self.tree_center[0], self.tree_center[1], 0],
                self.crown_radius * self.lifecirc**0.5, dx, dy)
        self.crown_node_ids = pointsTree[1]
        coords = pointsTree[0]
        self.crown_node_heights = []
        for i in range(len(self.crown_node_ids)):
            a = (self.tree_center-coords[i][:2])
            h = ((4 * self.crown_radius**2 - np.dot(a, a)) ** 0.5
                 + self.stem_height) #Hoehe des Baums am Gridpunkt
            self.crown_node_heights.append(h)

    def createRootMesh(self):
        searchTree = MPF.MeshPointFinder(self.land_mesh.grid)
        dx, dy = self.land_mesh.dx, self.land_mesh.dy
        pointsTree = searchTree.findPointsWithinRadius(
                [self.tree_center[0], self.tree_center[1], 0],
                self.root_radius * self.lifecirc**0.5, dx, dy)
        self.root_mesh = MeshInteractor.MeshInteractor(self.root_mesh_name)
        self.root_mesh.CreateMeshFromPoints(pointsTree)
        ids = pointsTree[1]
        self.root_mesh.CreateMultipleTriangles()
        self.root_mesh_node_ids = ids
        self.shadows = np.zeros(len(self.root_mesh_node_ids))

    def areaConeMinusTriangle(self, l1, l2, h):
        ## This function needs a drawing to be understood:
        # l1, l2 are the two lengths of the triangle
        # triangle c is pependicular to the radius of circle
        # diff_to_box corresponds to location of intersection of radius and
        # triangle_c
        # gamma is calculated using cosin_theorem
        r = self.root_radius * self.lifecirc**0.5
        diff_to_box = (r-h)
        triangle_a = np.sqrt((r-l1)**2)
        triangle_b = np.sqrt((r-l2)**2)
        triangle_c = (np.sqrt(triangle_a**2-diff_to_box**2) +
                      np.sqrt(triangle_b**2-diff_to_box**2))
        gamma = np.arccos((triangle_a**2 + triangle_b**2 - triangle_c**2) /
                          (2 * triangle_a * triangle_b))
        area_cone = gamma * r**2/2.
        area_triangle = (r-h) * triangle_c / 2.
        return area_cone - area_triangle

    def getTotalRootMeshArea(self):
        bounds = self.land_mesh.grid.GetBounds()
        r = self.root_radius * self.lifecirc**0.5
        hx1 = max([- (self.tree_center[0] - r - bounds[0]), 0])
        hx2 = max([self.tree_center[0] + r - bounds[1], 0])
        hy1 = max([- (self.tree_center[1] - r - bounds[2]), 0])
        hy2 = max([self.tree_center[1] + r - bounds[3], 0])
        d11 = max([r - np.sqrt((r - hx1)**2 + (r - hy1)**2), 0])
        d21 = max([r - np.sqrt((r - hx2)**2 + (r - hy1)**2), 0])
        d22 = max([r - np.sqrt((r - hx2)**2 + (r - hy2)**2), 0])
        d12 = max([r - np.sqrt((r - hx1)**2 + (r - hy2)**2), 0])
        root_area = self.root_mesh.getMeshAreaForTriangles()
        root_area += self.areaConeMinusTriangle(d11, d21, hy1)
        root_area += self.areaConeMinusTriangle(d21, d22, hx2)
        root_area += self.areaConeMinusTriangle(d22, d12, hy2)
        root_area += self.areaConeMinusTriangle(d12, d11, hx1)
        return root_area

    def addRootParametersToRootMesh(self):
        ogs_weigths = 1./self.getTotalRootMeshArea()

        self.psi_const = (self.Psi_leaf_min +
                          (2 * self.crown_radius +
                           self.stem_height) * 9810) * ogs_weigths
        self.psi_sal = 85000 * 1000 * ogs_weigths
        self.rootSurfaceResistance()
        self.xylemResistance()
        R = self.root_surface_resistance + self.xylem_resistance
        self.root_mesh.AddPropertyVector(
                self.psi_const/R*1000, "constant", "double")
        self.root_mesh.AddPropertyVector(0, "coeff1", "double")
        self.root_mesh.AddPropertyVector(
                self.psi_sal/R*1000, "coeff2", "double")
        self.root_mesh.AddPropertyVector(0, "coeff3", "double")
        self.root_mesh.AddPropertyVector(self.root_radius, "r_root", "double")
        self.root_mesh.AddPropertyVector(self.stem_radius, "r_stem", "double")
        self.root_mesh.AddPropertyVector(self.lifecirc, "lifecirc", "double")
        self.root_mesh.AddPropertyVector(self.stem_height, "h_stem", "double")
        self.root_mesh.AddPropertyVector(
                self.crown_radius, "r_crown", "double")
        self.root_mesh.AddPropertyVector(1, "one", "double")
